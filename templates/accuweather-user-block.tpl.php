<div id="accuweather-user-block">
    <h3><?php print $data->local->city; ?></h3>
    <div class="current">
        <h5><?php print t('Current condition'); ?>:</h5>
        <div class="condition-info column">
            <?php print theme('imagecache', 'accu-thumb-medium', drupal_get_path('module', 'accuweather') . '/icons/' . $data->currentconditions->weathericon . '.png'); ?>
            <span class="condition"><?php print t($data->currentconditions->weathertext); ?></span>
        </div>
        <div class="weather-info column">
            <p><?php print t('Updated'); ?>: <strong><?php print $data->local->time; ?></strong></p>
            <p><?php print t('Temperature'); ?>: <strong><?php print $data->currentconditions->temperature; ?>°<?php print $data->units->temp; ?></strong></p>
            <p><?php print t('RealFeel'); ?>: <strong><?php print $data->currentconditions->realfeel; ?>°<?php print $data->units->temp; ?></strong></p>
            <p><?php print t('Wind'); ?>: <strong><?php print $data->currentconditions->winddirection; ?> <?php print $data->currentconditions->windspeed; ?><?php print $data->units->speed; ?></strong></p>
        </div>
    </div>
    <div class="forecast column">
        <h5><?php print t('Forecast'); ?>:</h5>
        <?php for($i=1; $i<7; $i++): ?>
        <div class="day">
            <span><?php print t($data->forecast->day[$i]->daycode); ?></span>
            <?php print theme('imagecache', 'accu-thumb-small', drupal_get_path('module', 'accuweather') . '/icons/' . $data->forecast->day[$i]->daytime->weathericon . '.png'); ?>
            <strong><?php print $data->forecast->day[$i]->daytime->lowtemperature; ?>°<?php print $data->units->temp; ?>/<?php print $data->forecast->day[$i]->daytime->hightemperature; ?>°<?php print $data->units->temp; ?></strong>
        </div>
        <?php endfor; ?>
        <div class="clear"></div>
    </div>
    <?php print l(t('View details'), url('accuweather', array('absolute' => TRUE)), array('attributes' => array('id' => 'accuweather-full-details-link'))); ?>
    <div class="clear"></div>
</div>
