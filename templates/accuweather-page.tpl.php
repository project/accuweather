<div id="accuweather-current-weather-page">
  <h3><?php print $data->local->city; ?>, <?php print $data->local->state; ?></h3>
  <?php print $data->form; ?>
  <div class="current">
    <h5><?php print t('Current weather'); ?>: <span><?php print t($data->currentconditions->weathertext); ?></span></h5>
    <div class="condition">
      <img src="/<?php print drupal_get_path('module', 'accuweather') . '/icons/' . $data->currentconditions->weathericon . '.png'; ?>" alt="" />
    </div>
    <dl class="weather">
      <dt><?php print t('Updated'); ?>:</dt><dd><?php print $data->local->time; ?></dd>
      <dt><?php print t('Temperature'); ?>:</dt><dd><?php print $data->currentconditions->temperature; ?>°<?php print $data->units->temp; ?></dd>
      <dt><?php print t('RealFeel'); ?>:</dt><dd><?php print $data->currentconditions->realfeel; ?>°<?php print $data->units->temp; ?></dd>
      <dt><?php print t('Pressure'); ?>:</dt><dd><?php print $data->currentconditions->pressure; ?><?php print $data->units->pres; ?></dd>
      <dt><?php print t('Wind'); ?>:</dt><dd><?php print t($data->mappings['wind'][$data->currentconditions->winddirection]) . ', ' . $data->currentconditions->windspeed . $data->units->speed; ?></dd>
      <dt><?php print t('Visibility'); ?>:</dt><dd><?php print $data->currentconditions->windspeed . $data->units->dist; ?></dd>
      <dt><?php print t('Humidity'); ?>:</dt><dd><?php print $data->currentconditions->humidity; ?></dd>
      <dt><?php print t('Forecast on AccuWeather.com'); ?>:</dt><dd><a href="http://<?php print substr($data->currentconditions->url, strpos($data->currentconditions->url, '|', 1) + 1); ?>" class="target"><?php print t('Go to AccuWeather'); ?></a></dd>
      <dt><?php print t('Sunrise'); ?>:</dt><dd><?php print $data->planets->sun->{"@attributes"}->rise; ?></dd>
      <dt><?php print t('Sunset'); ?>:</dt><dd><?php print $data->planets->sun->{"@attributes"}->set; ?></dd>
      <dt><?php print t('UV radiation'); ?>:</dt><dd><?php print t($data->currentconditions->uvindex); ?></dd>
    </dl>
  </div>
  <div class="forecast">
    <h5><?php print t('Forecast'); ?>:</h5>
    <?php for ($i = 1; $i < 9; $i++): ?>
      <div class="day">
        <div class="condition">
          <h6><?php print t($data->forecast->day[$i]->daycode); ?> (<?php print format_date(strtotime($data->forecast->day[$i]->obsdate), 'custom', 'd-m-Y'); ?>)</h6>
          <?php print theme('imagecache', 'accu-thumb-big', drupal_get_path('module', 'accuweather') . '/icons/' . $data->forecast->day[$i]->daytime->weathericon . '.png'); ?>
          <?php print theme('imagecache', 'accu-thumb-big', drupal_get_path('module', 'accuweather') . '/icons/' . $data->forecast->day[$i]->nighttime->weathericon . '.png'); ?>
          <p><strong><?php print t('Daytime'); ?>:</strong> <?php print t($data->forecast->day[$i]->daytime->txtlong); ?></p>
          <p><strong><?php print t('Nighttime'); ?>:</strong> <?php print t($data->forecast->day[$i]->nighttime->txtlong); ?></p>
        </div>
        <dl class="weather">
          <dt><?php print t('Min temperature'); ?>: </dt><dd><?php print $data->forecast->day[$i]->daytime->lowtemperature; ?>°<?php print $data->units->temp; ?></dd>
          <dt><?php print t('Max temperature'); ?>: </dt><dd><?php print $data->forecast->day[$i]->daytime->hightemperature; ?>°<?php print $data->units->temp; ?></dd>
          <dt><?php print t('Wind'); ?>: </dt><dd><?php print t($data->mappings['wind'][$data->forecast->day[$i]->daytime->winddirection]) . ', ' . $data->currentconditions->windspeed . $data->units->speed; ?></dd>
          <dt><?php print t('Storm probability'); ?>: </dt><dd><?php print $data->forecast->day[$i]->daytime->tstormprob; ?>%</dd>
          <dt><?php print t('Sunrise'); ?>: </dt><dd><?php print $data->forecast->day[$i]->sunrise; ?></dd>
          <dt><?php print t('Sunset'); ?>: </dt><dd><?php print $data->forecast->day[$i]->sunset; ?></dd>
        </dl>
      </div>
    <?php endfor; ?>
  </div>
</div>