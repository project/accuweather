<?php

/**
 * Helper for mapping the wind direction from keycode to direction shortname
 */
function _accuweather_get_string_mappings() {
  
    $mappings['wind'] = array(
        'N' => t('north'),
        'NNE' => t('north-northeast'),
        'NE' => t('northeast'),
        'ENE' => t('east-northeast'),
        'E' => t('east'),
        'ESE' => t('east-southeast'),
        'SE' => t('southeast'),
        'SSE' => t('sout-southeast'),
        'S' => t('south'),
        'SSW' => t('south-southwest'),
        'SW' => t('southwest'),
        'WSW' => t('west-southwest'),
        'W' => t('west'),
        'WNW' => t('west-northwest'),
        'NW' => t('north-west'),
        'NNW' => t('north-northwest'));
    
    $mappings['units'] = array(
      'metric' => array(
        'wind' => 'km/h',
        'temperature' => 'C',
        'visibility' => 'km',
        'pressure' => 'mb',
      ),
      'imperial' => array(
        'wind' => 'mph',
        'temperature' => 'F',
        'visibility' => 'mi',
        'pressure' => 'in',
      ),
    );
    
    return $mappings;
}