<?php

/**
 * @file
 * Provides administration AccuWeather functions
 */

/**
 * AccuWeather admin settings form
 * @todo
 *   Select profile field for default "Profile" module
 */
function accuweather_admin_settings_form() {
  $form['profile_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile settings'),
  );
  $form['profile_settings']['accuweather_profile_module'] = array(
    '#title' => t('Profile module'),
    '#description' => t('Select profile module you\'re using on your website. Once the form is saved, if "Content profile" module is enabled and selected here, "Profile content type" field will show up.'),
    '#type' => 'select',
    '#default_value' => variable_get('accuweather_profile_module', ''),
    '#options' => array('' => t(' - select - ')),
  );
  if (module_exists('profile')) {
    $form['profile_settings']['accuweather_profile_module']['#options']['profile'] = 'Profile';
  }
  if (module_exists('content_profile')) {
    $form['profile_settings']['accuweather_profile_module']['#options']['content_profile'] = 'Content profile';
  }
  if (variable_get('accuweather_profile_module', '')) {
    $fields = array();
    switch (variable_get('accuweather_profile_module', '')) {
      case 'profile':
        // TODO
        break;
      case 'content_profile':
        $types = array();
        $profile_types = content_profile_get_types();
        if (!empty($profile_types)) {
          foreach ($profile_types as $type) {
            $types[$type->type] = $type->name;
          }
        }
        $form['profile_settings']['accuweather_content_profile_type'] = array(
          '#title' => t('Profile content type'),
          '#type' => 'select',
          '#default_value' => variable_get('accuweather_content_profile_type', ''),
          '#options' => array_merge_recursive(array('' => t(' - select - ')), $types),
          '#description' => t('Select profile content type beeing used on this website. After the form is saved, custom "Profile field" select will show up.'),
        );
        if ($type = variable_get('accuweather_content_profile_type', '')) {
          $content_type_fields = content_types($type);
          if (!empty($content_type_fields['fields'])) {
            foreach ($content_type_fields['fields'] as $key => $field) {
              $fields[$key] = $field['widget']['label'];
            }
          }
        }
        break;
    }
    if (!empty($fields)) {
      $form['profile_settings']['accuweather_profile_field'] = array(
        '#title' => t('Profile field'),
        '#type' => 'select',
        '#default_value' => variable_get('accuweather_profile_field', ''),
        '#options' => array('' => t(' - select - ')) + $fields,
        '#description' => t('Select profile field which is used to store users location (e.g. "City") in users profiles.'),
      );
    }
  }
  $form['accuweather_default_city'] = array(
    '#title' => t('Default location'),
    '#type' => 'textfield',
    '#default_value' => variable_get('accuweather_default_city', 'London'),
  );
  $form['accuweather_metric_system'] = array(
    '#title' => t('Metric units'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('accuweather_metric_system', 0),
    '#description' => t('If disabled, imperial units will be used.'),
  );
  $form['accuweather_use_default_css'] = array(
    '#title' => t('Use default CSS file'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('accuweather_use_default_css', TRUE),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return system_settings_form($form);
}